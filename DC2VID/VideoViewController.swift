//
//  PreviewViewController.swift
//  DC2VID
//
//  Created by Phatthanaphong on 22/5/2561 BE.
//  Copyright © 2561 Phatthanaphong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftyPlistManager

//import YouTubePlayer

class VideoViewController: UIViewController {

    var image : UIImage!
    var vid = [98: "https://www.youtube.com/watch?v=bWJWYq9zElw&feature=youtu.be",
               99: "https://www.youtube.com/watch?v=RtM8UY9AZVo&feature=youtu.be",
               100: "https://www.youtube.com/watch?v=D80VHn8id-8&feature=youtu.be",
               102: "https://www.youtube.com/watch?v=yUWXMGmsr-w&feature=youtu.be",
               103: "https://www.youtube.com/watch?v=4-sO22iR8Ds&feature=youtu.be",
               104: "https://www.youtube.com/watch?v=4-sO22iR8Ds&feature=youtu.be",
               107: "https://www.youtube.com/watch?v=1B3s2yawjHg&feature=youtu.be"]
    
    @IBOutlet weak var vwebView: UIWebView!
    @IBOutlet weak var photo: UIImageView!
    var textID = String()
    var timeAt = String()
    var initURL = String()
    var OCR = String()
    var filename = String()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //photo.frame = self.view.bounds
       
        //self.textID = "107"
        
        let imageData  = OpenCVWrapper.imageResize(self.image)
        //imageData = OpenCVWrapper.lineDetection(imageData)
        
        //send images to the server to perform searching
        //data is a playback location in the video
        //let saveFolder = "http://172.20.10.3:8080/IOSUpload/"
        
        guard let loadURL = SwiftyPlistManager.shared.fetchValue(for: "initURL", fromPlistWithName: "Data") else { return }
        initURL = loadURL as! String
        
        //let saveFolder = initURL+":8080/IOSUpload/"
        let saveFolder = "http://202.28.34.202/TextPrepPilot/gitweb/IOSUpload/"
        let data = UIImageJPEGRepresentation(imageData!, 1.0)
        requestWith(endUrl: saveFolder , imageData: data, parameters: ["fid":self.textID])


    }
    
    @objc func thumbsUpButtonPressed() {
        print("thumbs up button pressed")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /* This function uploads images to the server and pass the parameters to query a specfic document */
    func requestWith(endUrl: String, imageData: Data?, parameters: [String : Any], onCompletion: ((JSON?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        //let url =  initURL+":8080/IOSUpload/upload_image.php"
        //up to msu server
        let url = "http://202.28.34.202/TextPrepPilot/gitweb/IOSUpload/upload_image.php"
        
        let headers: HTTPHeaders = [
            /* "Authorization" if need*/
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "image.jpg", mimeType: "image/jpeg")
               
            }
            
        }, usingThreshold: UInt64.init(), to: url , method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    if let err = response.error{
                        onError?(err)
                        print(err)
                        return
                    }
                    print(response)
                    guard let jsonS = response.result.value as? [String: Any] else {
                        print("didn't get todo object as JSON from API")
                        if let error = response.result.error {
                            print("Error: \(error)")
                        }
                        return
                    }
                    guard let fname = jsonS["filename"] as? String else {
                        print("Could not get time from JSON")
                        return
                    }
                    guard let done = jsonS["done"] as? String else {
                        print("Could not get time from JSON")
                        return
                    }
                    
                    if (done != "true"){
                        return
                    }
                    
                    self.filename = "C:\\inetpub\\wwwroot\\TextPrepPilot\\gitweb\\IOSUpload\\"+fname
                    print(self.filename)

                    //let urlStringOCR = self.initURL+":9002/doocrfileJson"
                    //perform OCR from MSU Server
                    
                    let urlStringOCR = "http://202.28.34.202/textPrepPilot/OCRModule/process.php"
                    let sv = UIViewController.displaySpinner(onView: self.view)
                    Alamofire.request(urlStringOCR, method: .post, parameters: ["filename": self.filename],encoding: JSONEncoding.default, headers: nil).responseJSON {
                        response in
                        switch response.result {
                        case .success:
                            guard let json = response.result.value as? [String: Any] else {
                                print("didn't get todo object as JSON from API")
                                if let error = response.result.error {
                                    print("Error: \(error)")
                                    onCompletion?(nil)
                                }
                                return
                            }
                            // get and print the title
                            guard let ocrAns = json["ans"] as? String else {
                                print("Could not get ans from JSON")
                                return
                            }
                            print(ocrAns)
                            // rejecting some outliers
                            let Ttext = ocrAns.components(separatedBy: CharacterSet.newlines)
                            var text = [String()]
                            //if theres less than 10 characters, it will be rejected
                            for s in Ttext{
                                if (s.count > 10){
                                    text.append(s)
                                }
                            }
                            self.OCR = text[2]
                            print(self.OCR.count)
                            if(self.OCR.count > 5){
                                    //call Text Proc
                                    let urlStringTxt =  self.initURL+":9001/textProcessing"

                                    let h = self.initURL.substring(from: 7, to: self.initURL.count)

                                        Alamofire.request(urlStringTxt, method: .post, parameters: ["text":self.OCR, "host":h,"t_id":self.textID],encoding:
                                        JSONEncoding.default, headers: nil).responseJSON {
                                        response in
                                        switch response.result {
                                        case .success:
                                            print(response)
                                            guard let json = response.result.value as? [String: Any] else {
                                                print("didn't get todo object as JSON from API")
                                                if let error = response.result.error {
                                                    print("Error: \(error)")
                                                }
                                                return
                                            }
                                            let timeAt = json["ans"]
                                            let idx:Int? = Int(self.textID)
                                            //the format for playing back at aspecific time t of YouTube is &t=2m3s
                                            let url = URL(string: self.vid[idx!]!+"&t="+(timeAt as! String))
                                            UIViewController.removeSpinner(spinner: sv)
                                            //play avideo
                                            self.vwebView.loadRequest(URLRequest(url: url!))
                                            break
                                        case .failure(let error):
                                            print(error)
                                        }
                                    }
                            }else{
                                UIViewController.removeSpinner(spinner: sv)
                                self.displayMessgage(tileMsg: "failed", msg: "กรุณาลองใหม่อีกครั้ง")
                                return
                            }
                            
                        case .failure(let error):
                            print(error)
                            self.displayMessgage(tileMsg: "failed", msg: "กรุณาลองใหม่อีกครั้ง")
                        }
                        
                    }
                    onCompletion?(nil)
                   
                }
                
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    
    func  displayMessgage(tileMsg:String, msg:String){
        let alert = UIAlertController(title: tileMsg, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func backBtn(_ sender: Any) {
        self.dismiss(animated: true)
    }
}

//
// class extension
//
extension String {
    func substring(from: Int, to: Int) -> String {
        let start = index(startIndex, offsetBy: from)
        let end = index(start, offsetBy: to - from)
        return String(self[start ..< end])
    }
    
    func substring(range: NSRange) -> String {
        return substring(from: range.lowerBound, to: range.upperBound)
    }
}

extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}
