//
//  OpenCVWrapper.m
//  DC2VID
//
//  Created by Phatthanaphong on 23/5/2561 BE.
//  Copyright © 2561 Phatthanaphong. All rights reserved.
//


#import "OpenCVWrapper.h"
#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>
#import "OpenCVWrapper.h"

@implementation OpenCVWrapper
+(NSString *) openCVversionString{
    return [NSString stringWithFormat:@"OpenCV Version : %s",CV_VERSION];
}

+(UIImage*) nameGrayscale:(UIImage *)image{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    if (imageMat.channels() == 1) return image;
    
    cv::Mat gray;
    cv::cvtColor(imageMat, gray, CV_BGR2GRAY);
    return MatToUIImage(gray);
}

//resize image
+(UIImage*) imageResize:(UIImage *)image{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    
    cv::Mat resizedImage, output;
    
    cv::resize(imageMat, resizedImage, cv::Size(500,300));
    
    cv::Rect roi;
    roi.x = 0;
    roi.y = 100;
    roi.height = 100;
    roi.width = resizedImage.cols-1;
    
    output = resizedImage(roi);
     
    return MatToUIImage(output);
}

//detecting lines
+(UIImage*) lineDetection:(UIImage *) image{
    line lines[10];
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    cv::Mat gray;
    cv::Mat binary, output;
    cv::cvtColor(imageMat, gray, CV_BGR2GRAY);
    //thresholding with a specific threshold value
    //cv::threshold(gray, binary, 250, 255, CV_ADAPTIVE_THRESH_MEAN_C);
    cv::threshold(gray, binary, 150, 255, CV_THRESH_BINARY);
    
    //projection profiles
    
    int w = binary.cols;
    int h = binary.rows;
    int pix = 0.0;
    float sum_row[h];
    int idx_row[h];
    
    //Boolean flag = false;
    for(int i=0; i<h; i++)
        for(int j=0; j<w; j++){
            pix = binary.at<uchar>(i,j);
            //printf("%d \n",pix);
            sum_row[i] = sum_row[i]+(pix/255);
        }
    float ratio = 0.0;
    for(int i=0; i<h; i++){
        ratio = sum_row[i]/w;
        if(ratio > 99){
            idx_row[i] = 1;
        }else{
            idx_row[i] = 0;
        }
    }
    //printf("the number of rows is %d",binary.rows);
    
    for(int i=0; i<h; i++){
        if(idx_row[i] == 1){
            
        }
    }
    
    //binary = cv::cvarrToMat(&output2);
    
    return MatToUIImage(binary);
}

//normalization
+(UIImage*) normalization:(UIImage *) image{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    //to be coded
    return MatToUIImage(imageMat);
}


@end
